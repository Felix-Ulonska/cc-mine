local sides = redstone.getSides()

for i in pairs(sides) do
    if peripheral.getType(sides[i]) then
        rednet.open(sides[i])
    end
end

local weichen = {rednet.lookup('weichen')}
print(weichen)

for i, k in pairs(weichen) do
    rednet.send(k, 'open', 'weichen')
end