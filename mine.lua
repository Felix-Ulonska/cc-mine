local CHEST_SLOT = 15
local COAL_SLOT = 16

local function refuel()
	local success = true
	if turtle.getFuelLevel() < 20 then
		print("[+] Refuel")
		local oldSlot = turtle.getSelectedSlot()
		turtle.select(COAL_SLOT)
		success = turtle.refuel(1)
		turtle.select(oldSlot)
	end
	return success
end

local function check_full_inv()
	local full = true
	for i = 1, 16 do
		full = full and turtle.getItemDetail(i) ~= nil
	end
	return full
end

local function has_resources()
	return turtle.getItemDetail(CHEST_SLOT).name == "minecraft:chest"
end

local function build_chest() 
	turtle.turnRight()
	turtle.dig()
	turtle.digUp()
	turtle.up()
	turtle.dig()
	turtle.down()

	turtle.select(CHEST_SLOT)
	turtle.place()
	for i = 1, 14 do
		turtle.select(i)
		turtle.drop()
	end
	turtle.turnLeft()
	turtle.select(1)
end

local function step_forward(skipChest)
	skipChest = skipChest or false
	refuel()
	if check_full_inv() and not skipChest then
		build_chest()
	end
	print("[+] Diggin a step")
	if turtle.detect() then
		turtle.dig()
	end
	if turtle.detectUp() then
		turtle.digUp()
	end
	if not turtle.detectDown() then
		local curr_slot = 1
		while turtle.placeDown() == false and curr_slot < 15 do
			turtle.select(curr_slot)
			curr_slot = curr_slot + 1
		end
	end
	while turtle.forward() ~= true do
		turtle.dig()
	end
end

local function side_tunnel()
	local curr_pos = 0
	local step_length = 30
	for i = 1, step_length do
		if not has_resources() then
			print("Not enoght ressources")
			break
		end
		step_forward(i < 2)
		curr_pos = curr_pos + 1
		print('Curr Pos'..i)
	end
	turtle.turnLeft()
	turtle.turnLeft()
	while curr_pos ~= 0 do
		print('moving back'..curr_pos)
		step_forward()
		curr_pos = curr_pos - 1
	end
	print("Finished")
end

repeat
	step_forward()
	step_forward()
	step_forward(true)
	turtle.turnLeft()
	side_tunnel()
	print("bask to start")
	side_tunnel()
	turtle.turnRight()
until not has_resources()

